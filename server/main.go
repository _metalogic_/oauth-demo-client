/*
This is an example application to demonstrate parsing an ID Token.
*/
package main

import (
	"encoding/json"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/_metalogic_/config"
	"bitbucket.org/_metalogic_/log"
	oidc "github.com/coreos/go-oidc"
	"github.com/google/uuid"
	"golang.org/x/net/context"
	"golang.org/x/oauth2"
)

var (
	clientID       string
	clientSecret   string
	redirectDEV    string
	redirectPVW    string
	idpEPBC        string
	stateDEV       string
	statePVW       string
	stateGitLab    string
	devSessions    map[string]*json.RawMessage
	pvwSessions    map[string]*json.RawMessage
	gitlabSessions map[string]*oidc.UserInfo
)

type provider struct {
	config   oauth2.Config
	provider *oidc.Provider
	verifier *oidc.IDTokenVerifier
}

func (p *provider) authCodeURL(state string) string {
	return p.config.AuthCodeURL(state)
}

func (p *provider) exchange(ctx context.Context, code string) (*oauth2.Token, error) {
	return p.config.Exchange(ctx, code)
}

func (p *provider) verify(ctx context.Context, token string) (*oidc.IDToken, error) {
	return p.verifier.Verify(ctx, token)
}

func (p *provider) userinfo(ctx context.Context, token oauth2.TokenSource) (*oidc.UserInfo, error) {
	return p.provider.UserInfo(ctx, token)
}

func init() {
	devSessions = make(map[string]*json.RawMessage)
	pvwSessions = make(map[string]*json.RawMessage)
	gitlabSessions = make(map[string]*oidc.UserInfo)
}

func main() {

	log.SetLevel(log.DebugLevel)

	clientID = config.MustGetConfig("OAUTH_EPBC_CLIENT_ID")
	clientSecret = config.MustGetConfig("OAUTH_EPBC_CLIENT_SECRET")
	redirectDEV = config.MustGetConfig("OAUTH_DEV_REDIRECT_URL")
	redirectPVW = config.MustGetConfig("OAUTH_PVW_REDIRECT_URL")
	idpEPBC = config.MustGetConfig("IDP_EPBC")

	epbc := newProvider(idpEPBC, clientID, clientSecret, redirectDEV, "email")

	clientID = config.MustGetConfig("OAUTH_GITLAB_CLIENT_ID")
	clientSecret = config.MustGetConfig("OAUTH_GITLAB_CLIENT_SECRET")
	redirectDEV = config.MustGetConfig("OAUTH_GITLAB_REDIRECT_URL")

	gitlab := newProvider("https://gitlab.com", clientID, clientSecret, redirectDEV, "email")

	http.HandleFunc("/epbc-dev", func(w http.ResponseWriter, r *http.Request) {
		stateDEV = uuid.New().String()
		cookie, err := r.Cookie("epbc-dev-session")
		if err != nil { // cookie not found
			log.Debug("session cookie not found: redirecting to EPBC OAuth service")
			http.Redirect(w, r, epbc.authCodeURL(stateDEV), http.StatusFound)
			return
		}
		if userinfo, ok := devSessions[cookie.Value]; ok {
			data, err := json.MarshalIndent(userinfo, "", "    ")
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			w.Write(data)
			return
		}
		// cookie was sent but session not found
		http.Redirect(w, r, epbc.authCodeURL(stateDEV), http.StatusFound)
	})

	http.HandleFunc("/oauth/redirect/epbc-dev", func(w http.ResponseWriter, r *http.Request) {
		// validate state agrees with what we sent
		if r.URL.Query().Get("state") != stateDEV {
			http.Error(w, "state did not match", http.StatusBadRequest)
			return
		}

		ctx := context.Background()
		code := r.URL.Query().Get("code")

		log.Debugf("callback received with code: %s", code)

		oauth2Token, err := epbc.exchange(ctx, code)
		if err != nil {
			http.Error(w, "Failed to exchange token: "+err.Error(), http.StatusInternalServerError)
			return
		}

		rawIDToken, ok := oauth2Token.Extra("id_token").(string)
		if !ok {
			http.Error(w, "No id_token field in oauth2 token.", http.StatusInternalServerError)
			return
		}

		idToken, err := epbc.verify(ctx, rawIDToken)
		if err != nil {
			http.Error(w, "Failed to verify ID Token: "+err.Error(), http.StatusInternalServerError)
			return
		}

		claims := new(json.RawMessage)
		if err := idToken.Claims(&claims); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		devSessions[code] = claims
		log.Infof("data: %s", claims)

		var cookie *http.Cookie
		if strings.Contains(redirectDEV, "localhost") {
			// for testing set short expiry
			cookie = &http.Cookie{Name: "epbc-dev-session", Value: code, Path: "/", Expires: time.Now().Add(60 * time.Second)}
		} else {
			cookie = &http.Cookie{Name: "epbc-dev-session", Value: code, Domain: "educationplannerbc.ca", Path: "/", Expires: time.Now().Add(60 * time.Second), Secure: true, HttpOnly: true}
		}

		http.SetCookie(w, cookie)
		http.Redirect(w, r, "/epbc-dev", http.StatusFound)
	})

	http.HandleFunc("/epbc-pvw", func(w http.ResponseWriter, r *http.Request) {
		stateDEV = uuid.New().String()
		cookie, err := r.Cookie("epbc-pvw-session")
		if err != nil { // cookie not found
			log.Debug("session cookie not found: redirecting to EPBC OAuth service")
			http.Redirect(w, r, epbc.authCodeURL(statePVW), http.StatusFound)
			return
		}
		if userinfo, ok := pvwSessions[cookie.Value]; ok {
			data, err := json.MarshalIndent(userinfo, "", "    ")
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			w.Write(data)
			return
		}
		// cookie was sent but session not found
		http.Redirect(w, r, epbc.authCodeURL(stateDEV), http.StatusFound)
	})

	http.HandleFunc("/oauth/redirect/epbc-pvw", func(w http.ResponseWriter, r *http.Request) {
		// validate state agrees with what we sent
		if r.URL.Query().Get("state") != stateDEV {
			http.Error(w, "state did not match", http.StatusBadRequest)
			return
		}

		ctx := context.Background()
		code := r.URL.Query().Get("code")

		log.Debugf("callback received with code: %s", code)

		oauth2Token, err := epbc.exchange(ctx, code)
		if err != nil {
			http.Error(w, "Failed to exchange token: "+err.Error(), http.StatusInternalServerError)
			return
		}

		rawIDToken, ok := oauth2Token.Extra("id_token").(string)
		if !ok {
			http.Error(w, "No id_token field in oauth2 token.", http.StatusInternalServerError)
			return
		}

		idToken, err := epbc.verify(ctx, rawIDToken)
		if err != nil {
			http.Error(w, "Failed to verify ID Token: "+err.Error(), http.StatusInternalServerError)
			return
		}

		claims := new(json.RawMessage)
		if err := idToken.Claims(&claims); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		devSessions[code] = claims
		log.Infof("data: %s", claims)

		var cookie *http.Cookie
		if strings.Contains(redirectDEV, "localhost") {
			// for testing set short expiry
			cookie = &http.Cookie{Name: "epbc-pvw-session", Value: code, Path: "/", Expires: time.Now().Add(60 * time.Second)}
		} else {
			cookie = &http.Cookie{Name: "epbc-pvw-session", Value: code, Domain: "educationplannerbc.ca", Path: "/", Expires: time.Now().Add(60 * time.Second), Secure: true, HttpOnly: true}
		}

		http.SetCookie(w, cookie)
		http.Redirect(w, r, "/epbc-pvw", http.StatusFound)
	})

	http.HandleFunc("/gitlab", func(w http.ResponseWriter, r *http.Request) {
		stateGitLab = uuid.New().String()
		cookie, err := r.Cookie("epbc-session")
		if err != nil { // cookie not found
			log.Info("session cookie not found: redirecting to GitLab OAuth")
			http.Redirect(w, r, gitlab.authCodeURL(stateGitLab), http.StatusFound)
			return
		}
		if userinfo, ok := gitlabSessions[cookie.Value]; ok {
			data, err := json.MarshalIndent(userinfo, "", "    ")
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			w.Write(data)
			return
		}
		// cookie was sent but session not found
		http.Redirect(w, r, gitlab.authCodeURL(stateGitLab), http.StatusFound)
	})

	http.HandleFunc("/oauth/redirect/gitlab", func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Query().Get("state") != stateGitLab {
			http.Error(w, "state did not match", http.StatusBadRequest)
			return
		}

		ctx := context.Background()
		code := r.URL.Query().Get("code")

		log.Debugf("calledback received with code: %s", code)

		oauth2Token, err := gitlab.exchange(ctx, code)
		if err != nil {
			http.Error(w, "Failed to exchange token: "+err.Error(), http.StatusInternalServerError)
			return
		}

		rawIDToken, ok := oauth2Token.Extra("id_token").(string)
		if !ok {
			http.Error(w, "No id_token field in oauth2 token.", http.StatusInternalServerError)
			return
		}

		idToken, err := gitlab.verify(ctx, rawIDToken)
		if err != nil {
			http.Error(w, "Failed to verify ID Token: "+err.Error(), http.StatusInternalServerError)
			return
		}

		userInfo, err := gitlab.userinfo(ctx, oauth2.StaticTokenSource(oauth2Token))
		if err != nil {
			http.Error(w, "Failed to get userinfo: "+err.Error(), http.StatusInternalServerError)
			return
		}

		gitlabSessions[code] = userInfo

		oauth2Token.AccessToken = "*REDACTED*"

		resp := struct {
			OAuth2Token   *oauth2.Token
			IDTokenClaims *json.RawMessage // ID Token payload is just JSON.
			Userinfo      *oidc.UserInfo
		}{oauth2Token, new(json.RawMessage), userInfo}

		if err := idToken.Claims(&resp.IDTokenClaims); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		data, err := json.MarshalIndent(resp, "", "    ")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		log.Infof("%s", data)

		var cookie *http.Cookie
		if strings.Contains(redirectDEV, "localhost") {
			// for testing set short expiry
			cookie = &http.Cookie{Name: "epbc-session", Value: code, Path: "/", Expires: time.Now().Add(60 * time.Second)}
		} else {
			cookie = &http.Cookie{Name: "epbc-session", Value: code, Domain: "educationplannerbc.ca", Path: "/", Expires: oauth2Token.Expiry, Secure: true, HttpOnly: true}
		}

		http.SetCookie(w, cookie)
		http.Redirect(w, r, "/gitlab", http.StatusFound)
	})

	log.Fatal(http.ListenAndServe(":8080", nil))
}

func newProvider(url, clientID, clientSecret, redirectURL string, scopes ...string) *provider {
	ctx := context.Background()

	p, err := oidc.NewProvider(ctx, url)
	if err != nil {
		log.Fatal(err)
	}
	oidcConfig := &oidc.Config{
		ClientID: clientID,
	}

	scopes = append([]string{oidc.ScopeOpenID, "profile"}, scopes...)

	return &provider{
		config: oauth2.Config{
			ClientID:     clientID,
			ClientSecret: clientSecret,
			Endpoint:     p.Endpoint(),
			RedirectURL:  redirectURL,
			Scopes:       scopes,
		},
		provider: p,
		verifier: p.Verifier(oidcConfig)}

}
