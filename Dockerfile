FROM golang:1.14 as builder

ENV GOPRIVATE github.com/EducationPlannerBC/*,bitbucket.org/_metalogic_/*

COPY ./ /build

WORKDIR /build/server

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o server .

FROM metalogic/alpine:latest

RUN adduser -u 25010 -g 'Application Runner' -D runner

WORKDIR /home/runner

COPY --from=builder /build/server/server .
COPY --from=builder /build/public/index.html .

USER runner

CMD ["./server"]

#HEALTHCHECK --interval=30s CMD /usr/local/bin/health http://localhost:8080/health

