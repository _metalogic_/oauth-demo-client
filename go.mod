module bitbucket.org/_metalogic_/oauth-demo-client

go 1.14

require (
	bitbucket.org/_metalogic_/config v1.0.7
	bitbucket.org/_metalogic_/log v1.3.5
	github.com/coreos/go-oidc v2.2.1+incompatible
	github.com/google/go-cmp v0.4.1 // indirect
	github.com/google/uuid v1.1.1
	github.com/pquerna/cachecontrol v0.0.0-20180517163645-1555304b9b35 // indirect
	github.com/stretchr/testify v1.6.1 // indirect
	golang.org/x/net v0.0.0-20200602114024-627f9648deb9
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	gopkg.in/square/go-jose.v2 v2.5.1 // indirect
)
