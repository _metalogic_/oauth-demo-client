## Go OAuth+OpenID Connect Web App Demo

This is an example Go application that uses Gitlab OpenID Connect to authenticate and obtain basic GitLab user info.

In order to run the application:

1. Register your new application on Gitlab : https://gitlab.com/settings/applications/new. In the "callback URL" field, enter "http://localhost:8080/oauth/redirect". Once you register, you will get a client ID and client secret.
2. Export the of `clientID` and `clientSecret` as environment variables OAUTH_DEMO_CLIENTID and OAUTH_DEMO_CLIENT_SECRET
4. Start the server by executing `server/server`
5. Navigate to http://localhost:8080 on your browser.
